#!/bin/bash
# ... by Ingo Juergensmann 
# https://codeberg.org/Windfluechter/check_friendica.sh

set -e

# PATH to local.config.php of Friendica: 
FRIENDICA_PATH=/var/www/net/nerdica.net/friendica
FRIENDICA_CONF=${FRIENDICA_PATH}/config/local.config.php
CONSOLECMD="sudo -u www-data ${FRIENDICA_PATH}/bin/console"

hostname=$(grep "'hostname'" ${FRIENDICA_CONF} | awk -v FS="'" '{print $4}')
username=$(grep "'username'" ${FRIENDICA_CONF} | awk -v FS="'" '{print $4}')
password=$(grep "'password'" ${FRIENDICA_CONF} | awk -v FS="'" '{print $4}')
database=$(grep "'database'" ${FRIENDICA_CONF} | awk -v FS="'" '{print $4}')

users=$(mysql -h ${hostname} -u ${username} --password=${password} -r --skip-column-names ${database} -e "select count(*) from user" )
regstate=$(${CONSOLECMD} config config register_policy | awk '{print $NF}')

MAX=950
MOD=925
MIN=900

# regstate: 
# 0 - registration closed
# 1 - registratoon moderated
# 2 - registration open

#echo $users $regstate
if [ \( ${users} -ge ${MAX} \) -a \( $regstate -eq 2 \) ]; then
	echo "max limit reached"
	${CONSOLECMD} config config register_policy 0
elif [ \( \( ${users} -ge ${MIN} \) -a  \( $regstate -eq 2 \) \) -o \( \( ${users} -le ${MOD} \) -a \( $regstate -eq 0 \) \) ]; then 
	echo "mod limit reached"
	${CONSOLECMD} config config register_policy 1
elif [ \( ${users} -lt ${MIN} \) -a \( \( $regstate -eq 0 \) -o \( $regstate -eq 1 \) \) ]; then 
	echo "min limit reached"
	${CONSOLECMD} config config register_policy 2
else
	#echo "within limits"
	sleep .1
fi

  